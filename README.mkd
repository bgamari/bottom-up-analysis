# Bottom-up analysis

This is a quick hack for easily sorting caller profiles by the number of times
a particular function was called by each caller.

For instance, if we want to know the top call-sites of `map` we might compile
our program with `-fprof-caller=*.map`, run it with `+RTS -p -pj`, and analyze
the resulting profile with:
```
$ cabal v2-run bottom-up-analysis -- --function=map ghc.prof
```

