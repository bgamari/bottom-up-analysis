{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

import qualified Data.Text as T
import Data.List
import Data.Ord
import Prof
import Options.Applicative as O
import Text.Layout.Table

args :: O.Parser (FilePath, String)
args =
  (,)
  <$> O.argument str (metavar "JSON" <> help "JSON profile file")
  <*> O.option str (long "function" <> short 'f' <> metavar "FUNC" <> help "function name")

main :: IO ()
main = do
  (profFile, function) <- O.execParser $ O.info (O.helper <*> args) mempty
  prof <- resolveProfile <$> readProfile profFile
  let flatten :: ProfileTree a -> [ProfileTree a]
      flatten pt = pt : foldMap flatten (ptChildren pt)

  let pattern = "calling " <> T.pack function

  let cols = map fixedLeftCol [10, 8, 30, 30, 45]
  putStrLn
    $ tableString cols unicodeS (titlesH ["entries", "CCID", "module", "label", "src loc"])
    $ (\rows -> [rowsG rows])
    $ map toRow
    $ sortBy (flip $ comparing ptEntries) 
    [ pt 
    | pt <- flatten prof
    , pattern `T.isInfixOf` ccLabel (ptCc pt)
    ]

toRow :: ProfileTree CostCentre -> Row String
toRow pt =
  [ show (ptEntries pt)
  , show ccid
  , T.unpack ccModule
  , T.unpack ccLabel
  , T.unpack ccSrcLoc
  ]
  where
    CostCentre { ccId = CCID ccid
               , ..
               } = ptCc pt
