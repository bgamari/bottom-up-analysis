{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE RecordWildCards #-}

module Prof
  ( Profile(..)
  , ProfileTree(..)
  , CostCentre(..)
  , CCID(..)
  , readProfile
  , resolveProfile
  ) where

import Data.Maybe
import Data.Text (Text)
import qualified Data.ByteString.Lazy as BSL
import qualified Data.Map.Strict as M
import Data.Aeson as A
import Data.Proxy
import GHC.Generics

class HasJSONOptions a where
  jsonOptions :: Proxy a -> A.Options
  jsonOptions _ = A.defaultOptions

newtype ViaJSONOptions a = ViaJSONOptions a

instance (Generic a, GFromJSON Zero (Rep a), HasJSONOptions a) => A.FromJSON (ViaJSONOptions a) where
  parseJSON = fmap ViaJSONOptions . A.genericParseJSON (jsonOptions (Proxy @a))

instance (Generic a, GToJSON' Value Zero (Rep a), GToJSON' Encoding Zero (Rep a), HasJSONOptions a) => A.ToJSON (ViaJSONOptions a) where
  toJSON (ViaJSONOptions x) = A.genericToJSON (jsonOptions (Proxy @a)) x
  toEncoding (ViaJSONOptions x) = A.genericToEncoding (jsonOptions (Proxy @a)) x

newtype CCID = CCID Int
  deriving stock (Eq, Ord, Show)
  deriving newtype (A.FromJSON, A.ToJSON)

data Profile
  = Profile { profileProgram :: Text
            , profileArguments :: [Text]
            , profileRtsArguments :: [Text]
            , profileCostCentres :: [CostCentre]
            , profileProfile :: ProfileTree CCID
            }
  deriving (Generic, Show)
  deriving (A.ToJSON, A.FromJSON) via ViaJSONOptions Profile

instance HasJSONOptions Profile where
  jsonOptions _ = A.defaultOptions { fieldLabelModifier = A.camelTo2 '_' . drop 7 }

resolveProfile :: Profile -> ProfileTree CostCentre
resolveProfile prof = go (profileProfile prof)
  where
    costCentres = M.fromList [ (ccId cc, cc) | cc <- profileCostCentres prof ]
    lookupCc k = fromMaybe (error "unknown cost-centre") $ M.lookup k costCentres
    go (ProfileTree{..}) = ProfileTree { ptCc = lookupCc ptCc
                                       , ptChildren = map go ptChildren
                                       , ..
                                       }

data ProfileTree a
  = ProfileTree { ptCc :: !a
                , ptEntries :: !Int
                , ptAlloc :: !Int
                , ptTicks :: !Int
                , ptChildren :: [ProfileTree a]
                }
  deriving (Generic, Show)

instance A.FromJSON (ProfileTree CCID) where
  parseJSON = withObject "profile tree" $ \o ->
    ProfileTree 
       <$> o .: "id"
       <*> o .: "entries"
       <*> o .: "alloc"
       <*> o .: "ticks"
       <*> o .: "children"

instance A.ToJSON (ProfileTree CCID) where
  toJSON (ProfileTree{..}) = object
    [ "id" .= ptCc
    , "entries" .= ptEntries
    , "alloc" .= ptAlloc
    , "ticks" .= ptTicks
    , "children" .= ptChildren
    ]

data CostCentre
  = CostCentre { ccId :: !CCID
               , ccLabel :: !Text
               , ccModule :: !Text
               , ccSrcLoc :: !Text
               , ccIsCaf :: !Bool
               }
  deriving (Generic, Show)
  deriving (A.ToJSON, A.FromJSON) via ViaJSONOptions CostCentre

instance HasJSONOptions CostCentre where
  jsonOptions _ = A.defaultOptions { fieldLabelModifier = A.camelTo2 '_' . drop 2 }

readProfile :: FilePath -> IO Profile
readProfile fname = do
  res <- A.eitherDecode <$> BSL.readFile fname
  case res of
    Left err -> fail err
    Right profile -> return profile
